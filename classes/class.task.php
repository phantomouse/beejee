<?php namespace BeeJee\Primitives;

require_once './exceptions/exception.objectNotExists.php';

use Exception;
use PDO;
use BeeJee\Exceptions\ObjectNotExistsException;

/**
 * Class Task
 *
 * Модель для работы с отдельным элементом "Задача"
 *
 * @package BeeJee\Primitives
 */
class Task
{

    private $id = 0;
    private $author = '';
    private $email = '';
    private $text = '';
    private $done = false;
    private $edited = false;


    /**
     * Task constructor.
     * @param PDO $DBH
     * @param int $id
     * @throws ObjectNotExistsException
     */
    function __construct(PDO $DBH = null, $id = null) {
        if ($DBH !== null && $id != null) $this->loadByID($DBH, $id);
    }


    /**
     * Загружает данные объекта из БД по его идентификатору
     * @param PDO $DBH
     * @param int $id
     * @throws ObjectNotExistsException - в случае, если объект с заданным id не найден в БД
     * @throws Exception - в случае, если идентификатор объекта задан некорректно
     */
    public function loadByID(PDO $DBH, $id) {
        if (!is_int($id) || $id <= 0)
            throw new Exception("Идентификатор задачи должен быть целым числом больше нуля");
        $stmt = $DBH->prepare('SELECT * FROM `bj_tasks` WHERE `id` = ?');
        $stmt->execute([$id]);
        if ( $stmt->rowCount() == 0 )
            throw new ObjectNotExistsException();
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }


    /**
     * Сохраняет/обновляет данные объекта в БД
     * @param PDO $DBH
     */
    public function save(PDO $DBH) {
        if ($this->id == 0) {
            $stmt = $DBH->prepare('INSERT INTO `bj_tasks` (`author`, `email`, `text`, `done`, `edited`) VALUES (?, ?, ?, ?, ?)');
            $stmt->execute([$this->author, $this->email, $this->text, (int)$this->done, (int)$this->edited]);
        } else {
            $stmt = $DBH->prepare('UPDATE `bj_tasks` SET `author` = ?, `email` = ?, `text` = ?, `done` = ?, `edited` = ? WHERE `id` = ?');
            $stmt->execute([$this->author, $this->email, $this->text, (int)$this->done, (int)$this->edited, (int)$this->id]);
        }
    }


    /**
     * Представляет объект в виде простого массива
     * @return array
     */
    public function asArray() {
        return [
            'id' => $this->id,
            'author' => $this->author,
            'email' => $this->email,
            'text' => $this->text,
            'done' => $this->done,
            'edited' => $this->edited
        ];
    }


    public function __get($name) {
        if (property_exists($this, $name)) return $this->$name;
        return null;
    }


    /**
     * @param $name
     * @param $value
     * @throws Exception - в случае задания некорректного значения для поля класса
     */
    public function __set($name, $value) {
        switch ($name) {
            case 'id':
                if (!is_int($value) || $value <= 0)
                    throw new Exception("Идентификатор задачи должен быть целым числом больше нуля");
                $this->id = $value;
                break;
            case 'author':
                if (!is_string($value) || mb_strlen($value) < 2 || mb_strlen($value) > 50)
                    throw new Exception("Имя автора задачи должно быть строкой длиной не менее 2 и не более 50 символов");
                $this->author = filter_var($value, FILTER_SANITIZE_STRING);
                break;
            case 'email':
                if (!is_string($value) || mb_strlen($value) > 100)
                    throw new Exception("Адрес электронной почты должен быть строкой и содержать не более 100 символов");
                if (!preg_match("/^.+@.+\..+$/", $value))
                    throw new Exception("Адрес электронной почты не соответствует стандарту");
                $this->email = filter_var($value, FILTER_SANITIZE_STRING);
                break;
            case 'text':
                if (!is_string($value) || mb_strlen($value) < 3)
                    throw new Exception("Описание задачи должно быть строкой длиной не менее 3 символов");
                $this->text = filter_var($value, FILTER_SANITIZE_STRING);
                break;
            case 'done':
                if (is_string($value))
                    $this->done = $value == '1' ? true : false;
                else if (is_int($value))
                    $this->done = $value == 1 ? true : false;
                else if (is_bool($value))
                    $this->done = $value;
                else
                    throw new Exception("Значение поля done задано некорректно");
                break;
            case 'edited':
                if (is_string($value))
                    $this->edited = $value == '1' ? true : false;
                else if (is_int($value))
                    $this->edited = $value == 1 ? true : false;
                else if (is_bool($value))
                    $this->edited = $value;
                else
                    throw new Exception("Значение поля edited задано некорректно");
                break;
            default:
                throw new Exception("Класс не содержит поля \"$name\"");
        }

    }

}