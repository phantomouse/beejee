SET NAMES 'utf8';

CREATE TABLE bj_tasks (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  author varchar(50) NOT NULL,
  email varchar(100) DEFAULT NULL,
  text text DEFAULT NULL,
  done tinyint(1) NOT NULL DEFAULT 0,
  edited tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;