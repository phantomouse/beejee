<?php namespace BeeJee\Exceptions;

/**
 * Class ObjectNotExistsException
 *
 * Исключения этого типа выбрасываются при попытке загрузить из БД данные о несуществующем объекте
 *
 * @package BeeJee\Exceptions
 */
class ObjectNotExistsException extends \Exception {}