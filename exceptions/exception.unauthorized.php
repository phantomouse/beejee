<?php namespace BeeJee\Exceptions;

/**
 * Class UnauthorizedException
 *
 * Исключения этого типа выбрасываются при попытке совершить действие, разрешённое только для авторизованных
 * пользователей
 *
 * @package BeeJee\Exceptions
 */
class UnauthorizedException extends \Exception {

    function __constructor($message) {
        if (!$message) $this->$message = 'Авторизируйтесь в системе для выполнения этого действия';
    }

}