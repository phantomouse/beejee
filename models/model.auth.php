<?php namespace BeeJee\Models;

/**
 * Логика осуществления авторищзации пользователей
 */
class AuthModel
{

    /**
     * Аутентификация пользователя в системе
     * @param array $CFG - конфигурация приложения
     * @param string $username
     * @param string $password
     * @return array
     */
    static function logon($CFG, $username = '', $password = '') {

        if ($username == $CFG['admin']['username'] && $password == $CFG['admin']['password']) {
            $_SESSION['auth'] = true;
            return ['code' => 1];
        }

        else {
            $_SESSION['auth'] = false;
            return ['code' => 0, 'message' => 'Имя пользователя или пароль не верны'];
        }

    }


    /**
     * Выход пользователя из системы
     * @return array
     */
    static function logoff() {

        $_SESSION['auth'] = false;
        return ['code' => 1];

    }


    /**
     * Возвращает true в случае, если текущий пользователь авторизован
     * @return boolean
     */
    static function privileged() {
        return $_SESSION['auth'];
    }

}