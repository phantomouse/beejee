<?php namespace BeeJee\Models;

require_once "./classes/class.task.php";
require_once "./exceptions/exception.unauthorized.php";

use PDO;
use Exception;
use BeeJee\Exceptions\ObjectNotExistsException;
use BeeJee\Exceptions\UnauthorizedException;
use BeeJee\Primitives\Task;

/**
 * Модель управляет логикой работы с задачами
 */
class TasksModel
{

    /**
     * Настройки модели
     */
    const PAGE_LIMIT = 3;       # Количество элементов на страниу списка задач

    /**
     * Способы сортировки списка задач
     */
    const ORDERING_AUTHOR = 0;  # Способ сортировки: по имени автора
    const ORDERING_EMAIL = 1;   # Способ сортировки: по e-mail автора
    const ORDERING_STATUS = 2;  # Способ сортировки: по статусу задачи


    /**
     * Возвращает данные одной задачи для редактирования
     * @param PDO $DBH
     * @param $id
     * @return array
     * @throws UnauthorizedException
     */
    static function task(PDO $DBH, $id) {

        if (!AuthModel::privileged())
            throw new UnauthorizedException();

        try {
            $task = new Task($DBH, $id);
            return ['code' => 1, 'data' => $task->asArray(), 'privileged' => AuthModel::privileged()];
        }

        catch(ObjectNotExistsException $e) {
            return ['code' => 0, 'message' => "Задача с идентификатором \"$id\" не найдена"];
        }

    }


    /**
     * Возвращает список задач
     * @param PDO $DBH
     * @param int $pageNumber - номер отображаемой страницы списка
     * @param int $orderField - поле сортировки
     * @param bool $orderAscending - направление сортировки
     * @return array
     */
    static function listPage(PDO $DBH, $pageNumber = 1, $orderField = TasksModel::ORDERING_AUTHOR, $orderAscending = true) {

        $offset = ($pageNumber - 1) * TasksModel::PAGE_LIMIT;

        switch($orderField){
            case TasksModel::ORDERING_EMAIL:
                $ordering = '`email`';
                break;
            case TasksModel::ORDERING_STATUS:
                $ordering = '`done`';
                break;
            default:
                $ordering = '`author`';
        }
        if (!$orderAscending) $ordering .= ' DESC';

        $stmt = $DBH->prepare(sprintf(
            "SELECT `id`, `author`, `text`, `email`, `done`, `edited` FROM `bj_tasks` ORDER BY %s LIMIT %d, %d",
            $ordering,
            $offset,
            TasksModel::PAGE_LIMIT
        ));
        $stmt->execute([]);
        $list = [];
        foreach ($stmt as $row) $list[] = $row;
        $pc = $DBH->query("SELECT COUNT(`id`) FROM `bj_tasks`")->fetchColumn();
        $pc = ceil( $pc / TasksModel::PAGE_LIMIT );
        return ['code' => 1, 'pageCount' => $pc, 'list' => $list, 'privileged' => AuthModel::privileged()];

    }


    /**
     * Добавляет новую задачу
     * @param PDO $DBH
     * @param $data
     * @return array
     * @throws Exception - в случае передачи некорректного набора данных
     */
    static function add(PDO $DBH, $data) {

        if (
            !property_exists($data, 'author') ||
            !property_exists($data, 'text') ||
            !property_exists($data, 'email'))
                throw new Exception('Модели передан некорректный набор данных');

        $task = new Task();
        $task->author = $data->author;
        $task->email = $data->email;
        $task->text = $data->text;
        $task->save($DBH);

        return ['code' => 1];

    }


    /**
     * Изменяет данные существующей задачи
     * @param PDO $DBH
     * @param $data
     * @return array
     * @throws UnauthorizedException - в случае отсутствия прав на совершение операции
     * @throws Exception - в случае передачи некорректного набора данных
     */
    static function modify(PDO $DBH, $data) {

        if (
            !property_exists($data, 'id') ||
            !property_exists($data, 'author') ||
            !property_exists($data, 'text') ||
            !property_exists($data, 'done') ||
            !property_exists($data, 'email'))
            throw new \Exception('Модели передан некорректный набор данных');

        if (!AuthModel::privileged())
            throw new UnauthorizedException();

        try {
            $task = new Task($DBH, $data->id);
            if ($task->text != $data->text) $task->edited = true;
            $task->author = $data->author;
            $task->email = $data->email;
            $task->text = $data->text;
            $task->done = $data->done;
            $task->save($DBH);
            return ['code' => 1];
        }

        catch (ObjectNotExistsException $e) {
            return ['code' => 0, 'message' => "Задача с идентификатором \"$data->id\" не найдена"];
        }

    }

}