<?php

/**
 * Контроллер приложения
 * Фактически, слой view приложения организован в форме клиентского JavaScript, но, рарумеется, можно реализовать
 * элементы view и на php. Например, для отображения данных в разных форматах для разных категорий клиентов (HTML,
 * JSON, BinaryData и т.п.).
 */

require_once "./models/model.auth.php";
require_once "./models/model.tasks.php";
require_once './config.php';

use BeeJee\Exceptions\UnauthorizedException;
use BeeJee\Models\AuthModel;
use BeeJee\Models\TasksModel;

session_start();
if (!isset($_SESSION['auth'])) $_SESSION['auth'] = false;

$DBH = new PDO(
    sprintf(
        '%s:host=%s;dbname=%s;charset=%s;port=%d',
        $CFG['db']['driver'],
        $CFG['db']['host'],
        $CFG['db']['name'],
        $CFG['db']['charset'],
        $CFG['db']['port']
    ),
    $CFG['db']['user'],
    $CFG['db']['password'],
    [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false
    ]
);

if (array_key_exists('action', $_GET))
    $action = $_GET['action'];
else if (array_key_exists('action', $_POST))
    $action = $_POST['action'];
else
    $action = '';

try {

    $request = file_get_contents('php://input');
    if ($request !== '') $request = json_decode($request);
    $response = [];

    switch ($action) {
        case 'list':
            $pageNumber = array_key_exists('page', $_GET) ? $_GET['page'] : 1;
            $orderField = array_key_exists('orderField', $_GET) ? (int)$_GET['orderField'] : TasksModel::ORDERING_AUTHOR;
            $orderAscending = array_key_exists('orderAscending', $_GET) ? ($_GET['orderAscending'] === '1' ? true : false) : true;
            $response = TasksModel::listPage($DBH, $pageNumber, $orderField, $orderAscending);
            break;
        case 'save':
            if ($request->id > 0) {
                $response = TasksModel::modify($DBH, $request);
            } else {
                $response = TasksModel::add($DBH, $request);
            }
            break;
        case 'task':
            if (!array_key_exists('id', $_GET))
                throw new Exception('Идентификатор задачи не передан на сервер');
            $response = TasksModel::task($DBH, (int)$_GET['id']);
            break;
        case 'logon':
            if (!array_key_exists('username', $_POST))
                throw new Exception('Имя пользователя не передано на сервер');
            if (!array_key_exists('password', $_POST))
                throw new Exception('Пароль пользователя не передан на сервер');
            $response = AuthModel::logon($CFG, $_POST['username'], $_POST['password']);
            break;
        case 'logoff':
            $response = AuthModel::logoff();
            break;
        default:
            throw new ErrorException("Запрошено выполнение неизвестной команды \"$action\"");
    }

    header('Content-Type: application/json');
    echo json_encode($response);

}

catch (UnauthorizedException $e) {
    header('Content-Type: application/json');
    echo json_encode(['code' => 403, 'message' => $e->getMessage()]);
}

catch (Throwable $e) {
    header('Content-Type: application/json');
    echo json_encode(['code' => 0, 'message' => $e->getMessage()]);
}