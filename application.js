import ListPage from "./js/page.list.js";
import EditorPage from "./js/page.editor.js";
import LoginPage from "./js/page.login.js";

/**
 * Основной класс модели
 */
export default class Application {

    /**
     * Запускает приложение
     */
    static start() {
        document.body.querySelector('#btnAddTask').addEventListener('click', (ev)=>{
            Application.showEditorForm();
        });
        document.body.querySelector('#btnLogin').addEventListener('click', Application.showLoginForm);
        document.body.querySelector('#btnLogout').addEventListener('click', Application.logout);
        Application.setAddButtonVisibility(true);
        Application.showList();
    }


    /**
     * Переключает приложение на страницу со списком задач
     */
    static showList() {
        ListPage.show();
    }


    /**
     * Переключает приложение на страницу с редактором задачи.
     * Если идентификатор задачи для редактирования не передан - считается, что форма открыта для создания новой задачи
     * @param {int} id - идентификатор задачи для редактирования
     */
    static showEditorForm(id = -1) {
        EditorPage.show(id)
    }


    /**
     * Переключает приложение на форму авторизации
     */
    static showLoginForm() {
        LoginPage.show()
    }


    /**
     * Завершает сеанс работы пользователя в системе
     */
    static logout() {
        fetch('controller.php?action=logoff', {
            credentials: 'include'
        }).then(response => {
            if (!response.ok) return Promise.reject(response);
            return response.text();
        }).then(data => {
            let json = JSON.parse(data);
            if (json.code > 0)
                Application.showList();
            else {
                alert(`Произошла ошибка:\n${json.message}`);
            }
        }).catch(response=>{
            alert(`При выполнении запроса к серверу произошла неожиданная ошибка:\n${response.status} ${response.statusText}`);
        });
    }


    /**
     * Устанавливает залоговок страницы приложения
     * @param {String} value
     */
    static setTitle(value = '') {
        document.getElementById('pageTitle').innerHTML = value;
    }


    /**
     * Переключает видимость кнопки возврата к списку задач
     * @param {boolean} val
     */
    static setBackerVisibility(val) {
        document.getElementById('btnBack').style.display = val ? 'flex' : 'none';
    }


    /**
     * Переключает видимость кнопки авторизации
     * @param {boolean} val
     */
    static setLoginButtonVisibility(val) {
        document.getElementById('btnLogin').style.display = val ? 'flex' : 'none';
        document.getElementById('btnLogout').style.display = !val ? 'flex' : 'none';
    }


    /**
     * Переключает видимость кнопки добавления новой задачи
     * @param {boolean} val
     */
    static setAddButtonVisibility(val) {
        document.getElementById('btnAddTask').style.display = val ? 'flex' : 'none';
    }

}