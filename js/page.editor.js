import Application from "/application.js";

export default class EditorPage {

    static show(id = -1) {

        Application.setTitle(id === null ? 'Новая задача' : 'Редактирование задачи');
        Application.setBackerVisibility(true);

        let dom = document.createElement('div');
        dom.classList.add('page', 'editor');
        document.getElementById('pageContent').innerHTML =
            `<form class="editor">
                <div class="field">Имя пользователя:<br/><input id="authorField" maxlength="50" /></div>
                <div class="field">E-mail:<br/><input id="emailField" maxlength="100" /></div>
                <div class="field">Описание задачи:<br/><textarea id="textField"></textarea></div>
                <div class="field" id="doneFieldContainer"><div><input type="checkbox" id="doneField" /> <label for="doneField">Задача выполнена</label></div></div>
                <input id="btnSubmit" type="submit" value="${id === null ? 'Добавить задачу' : 'Сохранить задачу'}" />
            </form>`;

        document.body.querySelector('.backer').addEventListener('click', (ev)=>{
            Application.showList();
        });

        document.body.querySelector('form.editor').addEventListener('submit', (ev)=>{
            ev.preventDefault();
            EditorPage.submit(id);
        });

        if (id !== -1) {

            fetch(`controller.php?action=task&id=${id}`, {
                credentials: 'include'
            }).then(response => {
                if (!response.ok) return Promise.reject(response);
                return response.text();
            }).then(data => {
                let json = JSON.parse(data);
                if (json.code === 1) {
                    document.getElementById('authorField').value = json.data.author;
                    document.getElementById('emailField').value = json.data.email;
                    document.getElementById('textField').value = json.data.text;
                    document.getElementById('doneField').checked = json.data.done;
                } else if (json.code === 403) {
                    Application.showLoginForm();
                } else {
                    alert(`Произошла ошибка:\n${json.message}`);
                }
            }).catch(response=>{
                alert(`При выполнении запроса к серверу произошла неожиданная ошибка:\n${response.status} ${response.statusText}`);
            });

        } else {
            document.getElementById('doneFieldContainer').style.display = 'none';
        }

    }


    static submit(id = -1) {

        let elem = {
            author: document.body.querySelector('#authorField'),
            email: document.body.querySelector('#emailField'),
            text: document.body.querySelector('#textField'),
            done: document.body.querySelector('#doneField'),
            submit: document.body.querySelector('#btnSubmit')
        };

        let data = {
            id: id,
            author: elem.author.value,
            email: elem.email.value,
            done: elem.done.checked,
            text: elem.text.value
        };

        if (data.author.length < 2) {
            alert('Имя пользователя должно состоять не менее чем из двух символов!');
            return;
        }

        let re = /^.+@.+\..+$/;
        if (data.email.length < 5 || !re.test(data.email)) {
            alert('Адрес электронной почты введён некорректно!');
            return;
        }

        if (data.text.length < 3) {
            alert('Описание задачи должно состоять не менее чем из трёх символов!');
            return;
        }

        elem.submit.disabled = elem.text.disabled = elem.email.disabled = elem.author.disabled = true;

        fetch('controller.php?action=save', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify(data)
        }).then(response => {
            if (!response.ok) return Promise.reject(response);
            return response.text();
        }).then(data => {
            let json = JSON.parse(data);
            if (json.code > 0) {
                if (id === -1) alert(`Новая задача успешно добавлена!`);
                Application.showList();
            } else {
                alert(`Произошла ошибка:\n${json.message}`);
                elem.submit.disabled = elem.text.disabled = elem.email.disabled = elem.author.disabled = false;
            }
        }).catch(response=>{
            alert(`При выполнении запроса к серверу произошла неожиданная ошибка:\n${response.status} ${response.statusText}`);
            elem.submit.disabled = elem.text.disabled = elem.email.disabled = elem.author.disabled = false;
        });

    }

}