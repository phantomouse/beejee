import Application from "/application.js";

export default class LoginPage {

    static show() {

        document.getElementById('pageContent').innerHTML =
            `<form class="auth">
                <div class="field">Имя пользователя:<br/><input id="usernameField" /></div>
                <div class="field">Пароль:<br/><input id="passwordField" type="password" /></div>
                <input id="btnSubmit" type="submit" value="Войти" />
            </form>`;

        document.body.querySelector('form.auth').addEventListener('submit', (ev)=>{
            ev.preventDefault();
            LoginPage.submit();
        });

    }


    static submit() {

        let elem = {
            username: document.body.querySelector('#usernameField'),
            password: document.body.querySelector('#passwordField'),
            submit: document.body.querySelector('#btnSubmit')
        };

        if (elem.username.value === '') {
            alert('Имя пользователя не должно быть пустым!');
            return;
        }

        if (elem.password.value === '') {
            alert('Пароль пользователя не должен быть пустым!');
            return;
        }

        elem.submit.disabled = elem.username.disabled = elem.password.disabled = true;

        fetch('controller.php?action=logon', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8' },
            body: `username=${elem.username.value}&password=${elem.password.value}`
        }).then(response => {
            if (!response.ok) return Promise.reject(response);
            return response.text();
        }).then(data => {
            let json = JSON.parse(data);
            if (json.code > 0)
                Application.showList();
            else {
                alert(`Произошла ошибка:\n${json.message}`);
                elem.submit.disabled = elem.username.disabled = elem.password.disabled = false;
            }
        }).catch(response=>{
            alert(`При выполнении запроса к серверу произошла неожиданная ошибка:\n${response.status} ${response.statusText}`);
            elem.submit.disabled = elem.username.disabled = elem.password.disabled = false;
        });

    }

}