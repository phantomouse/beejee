import Application from "/application.js";

export default class ListPage {

    static get ORDERING_NAME() { return '0' }
    static get ORDERING_EMAIL() { return '1' }
    static get ORDERING_STATUS() { return '2' }

    static show() {

        if (sessionStorage.getItem('orderField') === null)
            sessionStorage.setItem('orderField', ListPage.ORDERING_NAME);
        if (sessionStorage.getItem('orderAscending') === null)
            sessionStorage.setItem('orderAscending', '1');
        if (sessionStorage.getItem('currentPage') === null)
            sessionStorage.setItem('currentPage', '1');

        Application.setTitle('Задачи');
        Application.setBackerVisibility(false);

        ListPage.retrieveData( parseInt( sessionStorage.getItem('currentPage') ) );

    }


    static retrieveData(page = 1) {

        sessionStorage.setItem('currentPage', page.toString());
        let orderField = sessionStorage.getItem('orderField');
        let orderAscending = sessionStorage.getItem('orderAscending');

        fetch(`controller.php?action=list&page=${page}&orderField=${orderField}&orderAscending=${orderAscending}`)
            .then(response => {
                if (!response.ok) return Promise.reject(response);
                return response.text();
            }).then(data => {
                let json = JSON.parse(data);
                if (json.code > 0) {
                    ListPage.displayData(json);
                } else {
                    alert(`Произошла ошибка:\n${json.message}`);
                }
            }).catch(response=>{
                alert(`При выполнении запроса к серверу произошла неожиданная ошибка:\n${response.status} ${response.statusText}`);
            });

    }


    static displayData(data = []) {

        document.getElementById('pageContent').innerHTML =
            `<div class="content" id="taskList"></div>
             <div class="footer">
                 <div class="order" id="orderList"></div>
                 <div class="pager" id="pagerList"></div>
             </div>`;

        document.body.querySelector('#taskList').addEventListener('click', ListPage.evTaskListClick);
        document.body.querySelector('.footer > .order').addEventListener('click', ListPage.evOrderingButtonClick);
        document.body.querySelector('.footer > .pager').addEventListener('click', ListPage.evPageButtonClick);

        let listNode = document.getElementById('taskList');
        let pagerNode = document.getElementById('pagerList');
        let orderNode = document.getElementById('orderList');
        if (!(listNode instanceof HTMLElement)) return;

        if (data.list.length === 0) {
            listNode.innerHTML = '<div class="empty">Список задач пуст</div>';
        }

        for(let o of data.list) {
            let elem = document.createElement('button');
            elem.className = 'card';
            elem.dataset.taskId = o.id;
            elem.innerHTML =
                `<div class="author">#${o.id} От: ${o.author}</div>
                <a href="mailto:${o.email}" class="email">${o.email}</a>
                <div class="text">${o.text}</div>
                <div class="tags"></div>`;
            if (o.done) {
                elem.classList.add('done');
                let tag = document.createElement('div');
                tag.className = 'done';
                tag.innerHTML = '<div class="icon">✓</div>Выполнена';
                elem.querySelector('.tags').appendChild(tag);
            }
            if (o.edited) {
                let tag = document.createElement('div');
                tag.className = 'edited';
                tag.innerHTML = '<div class="icon">🖉</div>Изменена администратором';
                elem.querySelector('.tags').appendChild(tag);
            }
            elem.disabled = !data.privileged;
            listNode.appendChild(elem);
        }

        let currentPage = parseInt(sessionStorage.getItem('currentPage'));
        for(let n = 1; n <= data.pageCount; n++) {
            let elem = document.createElement('button');
            elem.innerHTML = elem.dataset.number = `${n}`;
            if (currentPage === n) elem.disabled = true;
            pagerNode.appendChild(elem);
        }

        orderNode.innerHTML =
            `Сортировка:<button data-field="${ListPage.ORDERING_NAME}">По имени</button>
            <button data-field="${ListPage.ORDERING_EMAIL}">По e-mail</button>
            <button data-field="${ListPage.ORDERING_STATUS}">По статусу</button>`;
        orderNode
            .querySelector(`button[data-field="${sessionStorage.getItem('orderField')}"]`)
            .classList
            .add('sel', sessionStorage.getItem('orderAscending') === '1' ? 'asc' : 'desc');

        Application.setLoginButtonVisibility(!data.privileged);

    }


    static evPageButtonClick(ev) {
        if (ev.target.tagName !== 'BUTTON') return;
        ListPage.retrieveData(parseInt(ev.target.dataset.number));
    }


    static evOrderingButtonClick(ev) {
        if (ev.target.tagName !== 'BUTTON') return;
        if (sessionStorage.getItem('orderField') === ev.target.dataset.field) {
            sessionStorage.setItem('orderAscending',
                sessionStorage.getItem('orderAscending') === '1' ? '0' : '1'
            );
        } else {
            sessionStorage.setItem('orderField', ev.target.dataset.field);
            sessionStorage.setItem('orderAscending', '1');
        }
        ListPage.retrieveData( parseInt( sessionStorage.getItem('currentPage') ) );
    }


    static evTaskListClick(ev) {
        let id = -1;
        for (let o of ev.path) {
            if (o.disabled || o.classList.contains('content')) return;
            if (o.dataset.taskId !== undefined) {
                id = parseInt(o.dataset.taskId);
                break;
            }
        }
        ev.stopPropagation();
        if (id < 0) return;
        Application.showEditorForm(id);
    }

}